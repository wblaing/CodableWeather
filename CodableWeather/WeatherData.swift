//
//  WeatherData.swift
//  CodableWeather
//
//  Created by Blake Laing on 7/6/18.
//  Copyright © 2018 Blake Laing. All rights reserved.
//

import Foundation

class WeatherData: Codable {
    //Determine these two parameters from data pulled from Wunderground.com, OpenWeatherMap.org or similar using lattitude/longitude
    var stationID : String
    var stationElevation : Double

    var temperatureC: Double
    var temperatureDewpointC: Double
    var pressureMb: Double
    var stationRho : Double

    var windSpeed: Double
    var windDegrees: UInt
    var windString: String

    // MARK: - Codable
    private enum CodingKeys: String, CodingKey {
        case currentObservation = "current_observation"
        case displayLocation = "display_location"
        case stationID = "station_id"
        case stationElevation = "elevation"
        case temperatureC = "temp_c"
        case temperatureDewpointC = "dewpoint_c"
        case pressureMb = "pressure_mb"
        case windSpeed = "wind_kph"
        case windDegrees = "wind_degrees"
        case windString = "wind_string"
    }
    
    // Encoding
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var currentObservation = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .currentObservation)
        var displayLocation = currentObservation.nestedContainer(keyedBy: CodingKeys.self, forKey: .displayLocation)
        try currentObservation.encode(stationID, forKey: .stationID)
        try displayLocation.encode(stationElevation, forKey: .stationElevation)
        try currentObservation.encode(temperatureC, forKey: .temperatureC)
        try currentObservation.encode(temperatureDewpointC, forKey: .temperatureDewpointC)
        try currentObservation.encode(pressureMb, forKey: .pressureMb)
        try currentObservation.encode(windSpeed, forKey: .windSpeed)
        try currentObservation.encode(windDegrees, forKey: .windDegrees)
        try currentObservation.encode(windString, forKey: .windString)
    }
    
    // Decoding
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let currentObservation = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .currentObservation)
        let displayLocation = try currentObservation.nestedContainer(keyedBy: CodingKeys.self, forKey: .displayLocation)
        self.stationID = try currentObservation.decode(String.self, forKey: .stationID)
        self.stationElevation = Double(try displayLocation.decode(String.self, forKey: .stationElevation))!
        self.temperatureC = try currentObservation.decode(Double.self, forKey: .temperatureC)
        self.temperatureDewpointC = try currentObservation.decode(Double.self, forKey: .temperatureDewpointC)
        self.pressureMb = Double(try currentObservation.decode(String.self, forKey: .pressureMb))!
        self.windSpeed = try currentObservation.decode(Double.self, forKey: .windSpeed)
        self.windDegrees = try currentObservation.decode(UInt.self, forKey: .windDegrees)
        self.windString = try currentObservation.decode(String.self, forKey: .windString)
    }
    
    init() {
        stationID = ""
        stationElevation = 0.0

        temperatureC = 0.0
        temperatureDewpointC = 0.0
        pressureMb = 0.0
        stationRho = 0.0

        windSpeed = 0.0
        windDegrees = 0
        windString = ""
    }
}




