//
//  CustomView.swift
//  CodableWeather
//
//  Created by HauLe on 7/15/18.
//

import UIKit

@IBDesignable
open class CustomView: UIView {
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, contentLabel])
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.blue
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var contentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.blue
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    @IBInspectable
    public var title: String = "" {
        didSet {
            titleLabel.text = title
        }
    }
    
    @IBInspectable
    public var titleTextColor: UIColor = UIColor.white {
        didSet {
            titleLabel.textColor = titleTextColor
        }
    }
    
    @IBInspectable
    public var titleBackgroundColor: UIColor = UIColor.blue {
        didSet {
            titleLabel.backgroundColor = titleBackgroundColor
        }
    }
    
    @IBInspectable
    public var contentValue: Double = 0 {
        didSet {
            contentLabel.text = String(format: "%.\(precisionNumber)f", contentValue)
        }
    }
    
    @IBInspectable
    public var contentString: String = "" {
        didSet {
            if precisionNumber == 0 {
                contentLabel.text = contentString
            }
        }
    }
    
    @IBInspectable
    public var contentTextColor: UIColor = UIColor.blue {
        didSet {
            contentLabel.textColor = contentTextColor
        }
    }
    
    @IBInspectable
    public var contentBackgroundColor: UIColor = UIColor.white {
        didSet {
            contentLabel.backgroundColor = contentBackgroundColor
        }
    }

    @IBInspectable
    public var precisionNumber: Int = 0 {
        didSet {
            contentLabel.text = String(format: "%.\(precisionNumber)f", contentValue)
        }
    }
    
    @IBInspectable
    public var spacing: CGFloat = 0.0 {
        didSet {
            stackView.spacing = spacing
        }
    }
    
    // MARK: - UIView
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        // Stack View
        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        // Title Label
        let heightConstraint = NSLayoutConstraint(item: titleLabel, attribute: .height, relatedBy: .equal, toItem: contentLabel, attribute: .height, multiplier: 0.75, constant: 0.0)
        
        NSLayoutConstraint.activate([heightConstraint])
    }
}
